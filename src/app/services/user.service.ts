import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EmployerDTO} from '../classes/EmployerDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

   register(emp: EmployerDTO) {
      return this.http.post('http://localhost:8080/register', emp);
   }
   listEmployers() {
    return this.http.get('http://localhost:8080/users');
   }
}
