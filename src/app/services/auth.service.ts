import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  static isAuthenticated(): boolean {
    const token = sessionStorage.getItem('access_token');
    if (this.tockenIsNull(token)) {
      return false;
    }
    return true;
  }

  static getConnectedUser(): Employer {
    const token = sessionStorage.getItem('access_token');
    return this.getUserFromToken(token);
  }

  public static getUserFromToken(token: string): Employer {
    return null;
    /*
    const decodedUser = AuthService.decodeTocken(token);
    if (decodedUser) {
      return new Employer().constructorCopy(decodedUser);
    } else {
      return decodedUser;
    }
     */
  }

  static tockenIsNull(tockenStr) {
    return !(tockenStr != null && tockenStr !== '' && tockenStr.indexOf('.') > 0);
  }

  login(username: string, password: string) {
    const self = this;
    return this.http.post('http://localhost:8080/login', {username, password}, { responseType: 'text' }).pipe(
      map((res) => {
        const data = res ? JSON.parse(res) : '';
        if (AuthService.tockenIsNull(data.jwttoken)) {
          this.logout();
          window.location.replace('/login');
          return null;
        }
        sessionStorage.setItem('access_token', data.jwttoken);
        return data.jwttoken;
      })
    );
  }

  logout() {
    sessionStorage.clear();
  }
}
