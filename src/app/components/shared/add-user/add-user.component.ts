import { Component, OnInit } from '@angular/core';
import {EmployerDTO} from '../../../classes/EmployerDTO';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  employerDto: EmployerDTO;
  employer: Employer;
  registrationCode: number;

  constructor(private userService: UserService) {
    this.employerDto = new EmployerDTO();
    this.registrationCode = 0;
  }

  ngOnInit() {
  }

  register() {
    this.userService.register(this.employerDto).subscribe(res => {
      console.log(res);
      if (res) {
        this.registrationCode = 1;
        // this.employer = JSON.parse(res);
      }
    }, (error) => { this.registrationCode = 2; }
    );
  }

}
