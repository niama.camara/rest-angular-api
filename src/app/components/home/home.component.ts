import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  employers: any[] = [];

  constructor(private userService: UserService, private router: Router) {
    this.listEmployers();
  }

  listEmployers() {
    this.userService.listEmployers().subscribe(res => {
      console.log(res);
      // this.employers = JSON.parse(res);
    });
  }

  logout(e) {
    e.preventDefault();
    sessionStorage.clear();
    this.router.navigate(['login']);
  }

}
