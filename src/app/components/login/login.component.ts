import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router, RouterState, UrlSegment} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit , OnChanges {
  userName: string;
  password: string;

  constructor(private authService: AuthService, private http: HttpClient, private router: Router) { }

  login() {
    this.authService.login(this.userName, this.password).subscribe(res => {
      if (res) {
        this.router.navigate(['home']);
      }
    });
  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void { }

}
