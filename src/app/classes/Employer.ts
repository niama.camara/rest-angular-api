class Employer {
  id: number;
  username: string;
  password: string;
  email: string;
  profile: string;

  connstructor() {
    this.id = null;
    this.username = null;
    this.password = null;
    this.email = null;
    this.profile = null;
  }

  constructorCopy(emp: Employer) {
    this.id = emp.id;
    this.username = emp.username;
    this.password = emp.password;
    this.email = emp.email;
    this.profile = emp.profile;
    return this;
  }
}
