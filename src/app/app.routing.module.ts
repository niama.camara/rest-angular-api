import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {NgModule} from '@angular/core';
import {AuthGardService} from './services/auth-gard.service';
import {RegisterComponent} from './components/register/register.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent, canActivate : [AuthGardService]},
  { path: '**', redirectTo: '/home', pathMatch: 'full', canActivate : [AuthGardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
